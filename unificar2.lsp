;Daniel Hernandez Soria 70901834X
;Javier Iglesias Sanz 70896179J
(setq z1 ()) (setq z2 ())

(defun esVariable(_var)
    (format t " ->entro en esVariable~%")
    (cond 
        ((listp _var) (AND (equalp '2 (length _var)) (when (equalp '? (first _var)) t)))
        (t nil)
    )
)

(defun esAtomo(atomo)
    (format t " ->entro en esAtomo~%")
    (cond
        ((atom atomo) t)
        ((esVariable atomo) t)
        (t nil)
    )
)

(defun intercambia(_var1 _var2)
    (format t " ->entro en intercambia~%")
    (cond 
        ((esAtomo _var1) nil) 
        ((esAtomo _var2) (setf E1 _var2) (setf E2 _var1))
        (t nil)
    )
)

(defun pertenece(var1 var2) 
    (format t " ->entro en pertenece~%")
    (cond ((esAtomo var2) 
        (cond 
            ((equalp var1 var2) t) 
            (t nil)
        )) 
        (t (setf F2 (first var2)) 
            (setf T2 (rest var2)) 
            (cond
                ((equalp (pertenece var1 F2) 'FALLO) (pertenece var1 T2))
                (t t)
            )
        )
    )
)

    (defun aplicar (list1 list2)
        (format t " ->entro en aplicar~%")
        (setf list3 ())
        (cond
            ((equalp list2 'nada) (return-from aplicar 'nada))
            ((equalp list1 'nada) (return-from aplicar list2))
            ((not (listp list1)) (return-from aplicar 'fallo))
            (t (setf list2 (reverse list2)) 
                (dolist (var list2 list3)
                    (cond
                        ((esAtomo var) (cond 
                                            ((equalp var (first list1)) (setf list3 (append (last list1) list3)))
                                            (t (setf list3 (append (list var) list3)))
                                        )
                        )
                        (t (setf aux list3) (setf list3 (list (aplicar list1 var))) (setf list3 (append list3 aux)))
                    )
                )
            )
        )   
    )

(defun unificar (e1 e2)
    (progn () z1 z2
        (format t " ->entro en unificar~%")
        (cond
            ((and (esatomo e2) (not (esAtomo e1))) (intercambia e1 e2))
            ((esatomo e1)
                (cond
                    ((equalp e1 e2) (return-from unificar 'nada))
                    ((esVariable e1) (if (pertenece e1 e2) (return-from unificar 'fallo) (return-from unificar (list e2 e1))))
                    ((esvariable e2) (return-from unificar (list e1 e2)))
                    (t 'fallo)
                )
            )
            (t  
                (setf f1 (first e1)) (setf t1 (rest e1))
                (setf f2 (first e2)) (setf t2 (rest e2))
                (let ((z1 (unificar f1 f2)))
                    (cond
                        ((equalp z1 'fallo) 'fallo)
                        (t (setf g1 (aplicar z1 t1))
                            (setf g2 (aplicar z1 t2))
                             (setq z2 (unificar g1 g2))
                            (cond
                                ((equalp z2 'fallo) 'fallo)
                                (t (composicion z1 z2))
                            )
                        )
                    )
                )
            )        
        )
    )
)
(defun composicion (Z1 Z2)
  (setf resultado ())
  (setf seta1 (first Z1))
  (dolist (var Z2 resultado)
    (setf resultado seta1)
    (setf seta1 (aplicar var seta1))
  )
  (setf  Z1 (append (list seta1) (rest Z1)))
  (dolist (var_cenas Z2 resultado2)
    (cond
     ((NOT (aparece (first var_cenas) Z1)) (append (list Z1) var_cenas))
     (t nil)
    )
  )
)