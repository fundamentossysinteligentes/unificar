(defun variable(var)
    (cond
        ((AND (listp var) (equalp (first var) '?)) t)
        (T NIL)
    )
)
