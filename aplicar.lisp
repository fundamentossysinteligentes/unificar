(setq resultado ())
(defun aplicar (list1 list2)
    (print "entro en aplicar")
    (setf list3 ())
    (cond
        ((equalp list2 'nada) (return-from aplicar 'nada))
        ((equalp list1 'nada) (return-from aplicar list2))
        ((not (listp list1)) (return-from aplicar 'fallo))
        (t (setf list2 (reverse list2)) 
            (dolist (var list2 list3)
                (cond
                    ((atomo var) (cond 
                                        ((equalp var (first list1)) (setf list3 (append (last list1) list3)))
                                        (t (setf list3 (append (list var) list3)))
                                    )
                    )
                    (t (setf aux list3) (setf list3 (list (aplicar list1 var))) (setf list3 (append list3 aux)))
                )
            )
        )
    )   
)
;Podemos hacer que al principio se compruebe si (first var1) aparece 
;en var2, si aparece, solo hay que entrar en recursividad comprobando
;con un equalp que coincida con el (first var2)
;podriamos hacer 2 tipos de resultado, el LOCAL y el que se obtiene de
;la recursividad, los unimos y los devolvemos al nivel superior de recursividad.