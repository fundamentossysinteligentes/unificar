(defun aparece (v1 v2)
    (print v1)
    (cond
        ((OR (atomo v2) (esvariable v2)
            (cond
                ((equalp v1 v2) T)
                (T nil)
            )
        ))
        ((NOT (OR (atomo v2) (esvariable v2)))
            (cond
                ((aparece v1 (first v2)) T)
                ((aparece v1 (rest v2))  T)
                (T nil)
            )
        )
        (T nil)
    )
)